package fr.ulille.iuta.jmplace.syntax_tree;

public interface Node {
    int evaluate();
}
