package fr.ulille.iuta.jmplace.syntax_tree;

public class Value implements Node {

    final int value;
    public Value(int value) {
        this.value = value;
    }

    @Override
    public int evaluate() {
        return value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value value1 = (Value) o;
        return Double.compare(value1.value, value) == 0;
    }

    @Override
    public java.lang.String toString() {
        return Integer.toString(value);
    }
}
