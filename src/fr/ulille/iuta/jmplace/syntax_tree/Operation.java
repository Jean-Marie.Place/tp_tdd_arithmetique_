package fr.ulille.iuta.jmplace.syntax_tree;

import java.util.Objects;


public abstract class Operation implements Node {
    private final Node left;
    private final Node right;

    protected Operation(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int evaluate() {
        return  compute(left.evaluate(), right.evaluate());
    }

    protected abstract int compute(int a, int b) ;
    protected abstract String operator();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(left, operation.left) &&
                Objects.equals(right, operation.right);
    }

    public String toString() {
        return "(" + left + this.operator() + right + ")";
    }

	public static Node build(String operator, Node left, Node right) {
		switch (operator) {
			case "+" : return new Plus(left, right);
			case "-" : return new Minus(left, right);
			case "*" : return new Times(left, right);
			case "/" : return new Divide(left, right);
		}
		return null;
	}
}
