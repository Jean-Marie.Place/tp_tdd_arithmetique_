package fr.ulille.iuta.jmplace.syntax_tree;

public class Plus extends Operation {

    public Plus(Node left, Node right) {
        super(left, right);
    }

    @Override
    protected int compute(int a, int b) {
        return a + b;
    }

    @Override
    protected String operator() {
        return "+";
    }
}
