package fr.ulille.iuta.jmplace.syntax_tree;

public class Times extends Operation {

    public Times(Node left, Node right) {
        super(left, right);
    }

    @Override
    protected int compute(int a, int b) {
        return a * b;
    }

    @Override
    protected String operator() {
        return "*";
    }
}
