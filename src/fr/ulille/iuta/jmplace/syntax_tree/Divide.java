package fr.ulille.iuta.jmplace.syntax_tree;

public class Divide extends Operation {

    public Divide(Node left, Node right) {
        super(left, right);
    }

    @Override
    protected int compute(int a, int b) {
        return a / b;
    }

    @Override
    protected String operator() {
        return "/";
    }
}
