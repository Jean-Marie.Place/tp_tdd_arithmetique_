package fr.ulille.iuta.jmplace;

import fr.ulille.iuta.jmplace.syntax_tree.Divide;
import fr.ulille.iuta.jmplace.syntax_tree.Minus;
import fr.ulille.iuta.jmplace.syntax_tree.Node;
import fr.ulille.iuta.jmplace.syntax_tree.Plus;
import fr.ulille.iuta.jmplace.syntax_tree.Times;
import fr.ulille.iuta.jmplace.syntax_tree.Value;

public class EvaluatorNode extends Evaluator<Node> {

	@Override
	protected Node processValue(int v) {
        return new Value(v);
    }

	@Override
	protected Node processLeftRight(String operator, Node left, Node right) {
	       switch (operator) {
           case "+": return new Plus(left, right);
           case "-": return new Minus(left, right);
           case "*": return new Times(left, right);
           case "/": return new Divide(left, right);
       }
       throw new UnsupportedOperationException();
	}

}
