package fr.ulille.iuta.jmplace;

public class EvaluatorInteger extends Evaluator<Integer> {

	@Override
	protected Integer processValue(int value) {
		return value;
	}

	@Override
	protected Integer processLeftRight(String operator, Integer left, Integer right) {
		switch (operator) {
		case "+":
			return left + right;
		case "-":
			return left - right;
		case "*":
			return left * right;
		case "/":
			return left / right;
		default:
			throw new IllegalStateException("Unexpected value: " + operator);
		}
	}

}
