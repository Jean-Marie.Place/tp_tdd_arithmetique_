package fr.ulille.iuta.jmplace;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public abstract class Evaluator<Produce> {

    /**
     * The list of higher priority operators
     */
    protected static final Set<String> HIGHEST = new TreeSet<>(Arrays.asList("*", "/"));

    /**
     * Determine the end index of the factor starting at the start index of the token array.
     * A factor is a subexpression including only multiplicative operators ('*' and '/')
     * @param tokens the list of tokens (operands and operators)
     * @param start the index of the first operand of the subexpression (in the tokens list).
     *              it is assumed that this value is actually the start of the factor;
     *              in order the get the factor the longest
     *              if not, the beginning is left dismissed
     * @return the end index of the factor (or the length of the tokens array when the factor extends to the end)
     */
    protected int factorBounding(String[] tokens, int start) {
        int index = start+1;
        while ((index < tokens.length) && (HIGHEST.contains(tokens[index]))) {
            index += 2;
        }
        return index;
    }

    /**
     * Process an integer expression.
     * Allowed operators are '+', '-', '*' and '/'. '*' and '-' operators take precedence over '+' and '-'.
     * @param expression the expression to be analyzed
     * @return the result of processing the expression
     */
    public Produce processExpression(String expression) {
        String[] tokens = expression.trim().split(" ");
        Produce resultat;
        int start = 0;
        int end = factorBounding(tokens, start);
        resultat = processFactor(tokens, start, end);
        while (end < tokens.length) {
            String operator = tokens[end];
            start = end + 1;
            end = factorBounding(tokens, start);
            Produce right = processFactor(tokens, start, end);
            resultat = processLeftRight(operator, resultat, right);
        }
        return resultat;
    }

    /**
     * Process a subexpression (factor)
     * A factor is composed only of multiplicative operations (* and /).
     * the subexpression is specified by tokens, start and end parameters.
     * @param tokens the list of tokens (operands and operators)
     * @param start the index of the first operand of the subexpression (in the tokens list)
     * @param end the index <u>after</u> the last operand of the subexpression (or after the end of the whole expression)
     * @return the computed value for this subexpression
     */
	protected Produce processFactor(String[] tokens, int start, int end) {
		Produce resultat = processValue(Integer.parseInt(tokens[start]));
		int idx = start + 1;
		while (idx < end) {
			Produce operande = processValue(Integer.parseInt(tokens[idx + 1]));
			String operator = tokens[idx];
			resultat = processLeftRight(operator, resultat, operande);
			idx += 2;
		}
		return resultat;
	}

    /**
     * Process a single operation upon two values.
     * @param operator operator to be applyed.
     * allowed values are "+", "-", "*", "/". if neither of these values is given, an IllegalStateException is thrown.
     * @param left the left hand side value
     * @param right the right hand side value
     * @return the result of applying the operation on the two values
     */
	protected abstract Produce processLeftRight(String operator, Produce left, Produce right);

    /**
     * Process a single integer value.
     */
	protected abstract Produce processValue(int parseInt);

}
