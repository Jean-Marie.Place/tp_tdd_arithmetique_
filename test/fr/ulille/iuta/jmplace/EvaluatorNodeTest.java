package fr.ulille.iuta.jmplace;

import org.junit.jupiter.api.Test;

import fr.ulille.iuta.jmplace.syntax_tree.Operation;
import fr.ulille.iuta.jmplace.syntax_tree.Value;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class EvaluatorNodeTest {

	protected EvaluatorNode evaluator;
	
	@BeforeEach
	public void setup() {
		evaluator = new EvaluatorNode();
	}

    @Test
    public void OperatorsEquality() {
        assertEquals(Operation.build("+", null, null), Operation.build("+", null, null));
        assertEquals(new Value(16), new  Value(16));
    }

    @Test
    public void parseSimpleAddition() {
        assertEquals(Operation.build("+",
                new Value(12),
                new Value(56)),
                evaluator.processExpression("12 + 56"));
    }

    @Test
    public void parseExpressionWithPriority() {
        assertEquals(
                Operation.build("+",
                        new Value(12),
                        Operation.build("*",
                                new Value(56),
                                new Value(2)))
                , evaluator.processExpression("12 + 56 * 2"));
    }

    @Test
    public void evaluateParsedExpressionWithPriority() {
        assertEquals(124, evaluator.processExpression("12 + 56 * 2").evaluate());
    }

    @Test
    public void evaluateParsedExpressionWithDivide() {
        assertEquals(7, evaluator.processExpression("8 / 4 + 15 / 3").evaluate());
    }

    @Test
    public void evaluateParsedExpressionWithMinus() {
        assertEquals(1, evaluator.processExpression("10 - 27 / 3").evaluate());
    }

    @Test
    public void displayExpression() {
        assertEquals("(((2*3)+(7/2))-1)", evaluator.processExpression("2 * 3 + 7 / 2 - 1").toString());
    }

}