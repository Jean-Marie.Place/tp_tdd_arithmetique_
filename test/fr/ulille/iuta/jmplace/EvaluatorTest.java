package fr.ulille.iuta.jmplace;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class EvaluatorTest {

	protected static class EvaluatorEmpty extends Evaluator<Object> {

		@Override
		protected Object processLeftRight(String operator, Object left, Object right) {
			return null;
		}

		@Override
		protected Object processValue(int parseInt) {
			return null;
		}
		
	}

	protected EvaluatorEmpty evaluator;
	
	@BeforeEach
	public void setup() {
		evaluator = new EvaluatorEmpty();
	}

    @Test
    public void isolateOneSingleFactor() {
        assertEquals(3, evaluator.factorBounding("3 * 5".split(" "), 0));
    }

    @Test
    public void isolateOneDoubleFactor() {
        assertEquals(5, evaluator.factorBounding("3 * 5 / 2".split(" "), 0));
    }

    @Test
    public void isolateFirstFactorFromTwo() {
        assertEquals(5, evaluator.factorBounding("3 * 5 / 2 + 5 * 7".split(" "), 0));
    }

    @Test
    public void isolateSecondFactorFromTwo() {
        assertEquals(9, evaluator.factorBounding("3 * 5 / 2 + 5 * 7".split(" "), 6));
    }

    @Test
    public void isolateFactorsFromThree() {
        final String[] tokens = "3 * 5 / 2 + 5 * 7 + 12 - 7".split(" ");
        //                  idx: 0 1 2 3 4 5 6 7 8 9 10 1 2
        assertEquals(5, evaluator.factorBounding(  tokens, 0));
        assertEquals(9, evaluator.factorBounding(  tokens, 6));
        assertEquals(11, evaluator.factorBounding(  tokens, 10));
        assertEquals(13, evaluator.factorBounding(  tokens, 12));
    }

}