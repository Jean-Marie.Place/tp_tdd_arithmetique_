package fr.ulille.iuta.jmplace;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class EvaluatorIntegerTest {

	protected EvaluatorInteger evaluator;
	
	@BeforeEach
	public void setup() {
		evaluator = new EvaluatorInteger();
	}

    @Test
    public void singleIntegerValue() {
        assertEquals(12, evaluator.processExpression("12"));
    }

    @Test
    public void singleNegativeValue() {
        assertEquals(-15, evaluator.processExpression("-15"));
    }

    @Test
    public void simpleAddTwoPositiveValues() {
        assertEquals(37, evaluator.processExpression("30 + 7"));
    }

    @Test
    public void simpleAddWithOneSignedValues() {
        assertEquals(23, evaluator.processExpression("30 + -7"));
    }

    @Test
    public void simpleAddWithTwoSignedValues() {
        assertEquals(-37, evaluator.processExpression("-30 + -7"));
    }

    @Test
    public void simpleMultiplyTwoPositiveValues() {
        assertEquals(28, evaluator.processExpression("4 * 7"));
    }

    @Test
    public void simpleMultiplyWithOneSignedValues() {
        assertEquals(-210, evaluator.processExpression("30 * -7"));
    }

    @Test
    public void simpleMultiplyWithTwoSignedValues() {
        assertEquals(35, evaluator.processExpression("-5 * -7"));
    }

    @Test
    public void simpleIntegerDivide() {
        assertEquals(4, evaluator.processExpression("13 / 3"));
    }

    @Test
    public void simpleSubstract() {
        assertEquals(-6, evaluator.processExpression("1 - 7"));
    }

    @Test
    public void doubleAdd() {
        assertEquals(11, evaluator.processExpression("1 + 7 + 3"));
    }

    @Test
    public void doubleAddSubstract() {
        assertEquals(69, evaluator.processExpression("-1 + 73 - 3"));
    }

    @Test
    public void doubleMultiply() {
        assertEquals(4, evaluator.processExpression("2 * 6 / 3"));
    }

    @Test
    public void substract() {
        assertEquals(1, evaluator.processExpression("4 - 3"));
    }

    @Test
    public void spacesBefore() {
        assertEquals(124, evaluator.processExpression(" 124"));
    }

    @Test
    public void spacesAfter() {
        assertEquals(174, evaluator.processExpression("174  "));
    }

    @Test
    public void multiply_left_right_add() {
        assertEquals(14, evaluator.processExpression("2 + 3 * 4"));
    }

    @Test
    public void multiply_right_left_add() {
        assertEquals(10, evaluator.processExpression("2 * 3 + 4"));
    }

    @Test
    public void multiply_add_multiply() {
        assertEquals(26, evaluator.processExpression("2 * 3 + 4 * 5"));
    }

}